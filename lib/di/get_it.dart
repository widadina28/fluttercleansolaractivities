import 'package:get_it/get_it.dart';
import 'package:sun_flare_app/data/repository/geo_storm_repository_impl.dart';
import 'package:sun_flare_app/data/repository/solar_flare_repository_impl.dart';
import 'package:sun_flare_app/data/service/nasa_service.dart';
import 'package:sun_flare_app/domain/repository/geo_storm_repository.dart';
import 'package:sun_flare_app/domain/repository/solar_flare_repository.dart';
import 'package:sun_flare_app/domain/usecase/interactor/solar_activities_use_case_impl.dart';
import 'package:sun_flare_app/domain/usecase/solar_activities_use_case.dart';
import 'package:sun_flare_app/presentation/bloc/solar_activities_bloc/solar_activities_bloc.dart';

GetIt get = GetIt.I;
initGet() {
  get.registerFactory<SolarActivitiesUseCase>(
      () => SolarActivitiesUseCaseImpl());

  get.registerFactory<GeoStormRepository>(() => GeoStormRepositoryImpl(get()));
  get.registerFactory<SolarFlareRepository>(
      () => SolarFlareRepositoryImpl(get()));

  get.registerFactory<SolarActivitesBloc>(() => SolarActivitesBloc(get()));
  get.registerFactory<NasaService>(() => NasaService());
}
