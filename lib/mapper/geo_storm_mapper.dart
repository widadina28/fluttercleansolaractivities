import 'package:sun_flare_app/data/remote/geo_storm_response.dart';
import 'package:sun_flare_app/domain/models/geo_storm.dart';

extension GeoStormMapper on GeoStormResponse {
  GeoStorm toModel() {
    return GeoStorm(gstId: gstId, startTime: startTime);
  }
}
