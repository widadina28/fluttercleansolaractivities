import 'package:sun_flare_app/data/remote/solar_flare_response.dart';
import 'package:sun_flare_app/domain/models/solar_flare.dart';

extension SolarFlareMapper on SolarFlareResponse {
  SolarFlare toModel() {
    return SolarFlare(
        flrId: flrID,
        beginTime: beginTime,
        endTime: endTime,
        classType: classType,
        sourceLocation: sourceLocation);
  }
}
