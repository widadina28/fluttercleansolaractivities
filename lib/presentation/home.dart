import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sun_flare_app/di/get_it.dart';
import 'package:sun_flare_app/domain/models/solar_activities.dart';
import 'package:sun_flare_app/presentation/bloc/solar_activities_bloc/solar_activities_bloc.dart';

class Home extends StatefulWidget {
  // HomeState homeState;

  // Home({Key key, required this.homeState}) : super(key: key);
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  // final SolarActivitesBloc solarActivitesBloc = SolarActivitesBloc(get());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Solar Activities')),
      body: _buildSolarActivities(),
    );
  }

  Widget _buildSolarActivities() {
    return Container(
      margin: EdgeInsets.all(8.0),
      child: BlocProvider(
        create: (_) => SolarActivitesBloc(get())..add(GetSolarActivities()),
        child: BlocListener<SolarActivitesBloc, SolarActivitiesState>(
          listener: (context, state) {
            if (state is SolarActivitiesError) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(state.message!),
                ),
              );
            }
          },
          child: BlocBuilder<SolarActivitesBloc, SolarActivitiesState>(
            builder: (context, state) {
              if (state is SolarActivitiesInitial) {
                return _buildLoading();
              } else if (state is SolarActivitiesLoading) {
                return _buildLoading();
              } else if (state is SolarActivitiesLoaded) {
                return _buildView(context, state.solarActivities);
              } else if (state is SolarActivitiesError) {
                return Container();
              } else {
                return Container();
              }
            },
          ),
        ),
      ),
    );
  }

  Widget _buildView(BuildContext context, SolarActivities data) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text('Last Solar Flare Date: ${data.lastFlare.beginTime}'),
        Text('Last Geo Storm Date: ${data.lastStorm.startTime}'),
      ],
    );
  }

  Widget _buildLoading() => const Center(child: CircularProgressIndicator());
}
