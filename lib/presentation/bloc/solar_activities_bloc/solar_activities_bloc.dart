import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sun_flare_app/di/get_it.dart';
import 'package:sun_flare_app/domain/models/solar_activities.dart';
import 'package:sun_flare_app/domain/usecase/solar_activities_use_case.dart';

part 'solar_activities_event.dart';
part 'solar_activities_state.dart';

class SolarActivitesBloc
    extends Bloc<SolarActivitiesEvent, SolarActivitiesState> {
  final SolarActivitiesUseCase solarActivitiesUseCase;
  SolarActivitesBloc(this.solarActivitiesUseCase)
      : super(SolarActivitiesInitial()) {
    log("masuk1 ===");
    on<GetSolarActivities>((event, emit) async {
      log("masuk ===");
      try {
        emit(SolarActivitiesLoading());
        final data =
            await solarActivitiesUseCase.getLastSolarActivities(get(), get());
        emit(SolarActivitiesLoaded(data));
        log("data ===");
        log(data.toString());
      } catch (e) {
        log('error');
        log(e.toString());
        emit(SolarActivitiesError(e.toString()));
      }
    });
  }
}
