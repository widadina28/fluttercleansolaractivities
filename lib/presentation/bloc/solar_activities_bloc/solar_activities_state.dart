part of 'solar_activities_bloc.dart';

abstract class SolarActivitiesState extends Equatable {
  const SolarActivitiesState();
  @override
  List<Object?> get props => [];
}

class SolarActivitiesInitial extends SolarActivitiesState {}

class SolarActivitiesLoading extends SolarActivitiesState {}

class SolarActivitiesLoaded extends SolarActivitiesState {
  final SolarActivities solarActivities;
  const SolarActivitiesLoaded(this.solarActivities);
}

class SolarActivitiesError extends SolarActivitiesState {
  final String? message;
  const SolarActivitiesError(this.message);
}
