part of 'solar_activities_bloc.dart';

class SolarActivitiesEvent extends Equatable {
  const SolarActivitiesEvent();
  @override
  List<Object> get props => [];
}

class GetSolarActivities extends SolarActivitiesEvent {}
