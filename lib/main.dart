import 'package:flutter/material.dart';
import 'package:sun_flare_app/di/get_it.dart';
import 'package:sun_flare_app/presentation/home.dart';

void main() {
  initGet();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Solar Activities",
      debugShowCheckedModeBanner: false,
      home: Home(),
    );
  }
}
