import 'package:sun_flare_app/domain/models/solar_flare.dart';

abstract class SolarFlareRepository {
  Future<List<SolarFlare>> getFlares(
      {required DateTime from, required DateTime to});
}
