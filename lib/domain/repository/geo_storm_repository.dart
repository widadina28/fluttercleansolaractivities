import 'package:sun_flare_app/data/service/nasa_service.dart';
import 'package:sun_flare_app/domain/models/geo_storm.dart';

abstract class GeoStormRepository {
  Future<List<GeoStorm>> getStorms(
      {required DateTime from, required DateTime to});
}
