class SolarFlare {
  String? flrId = "";
  DateTime? beginTime;
  DateTime? endTime;
  String? classType = "";
  String? sourceLocation = "";

  SolarFlare(
      {required this.flrId,
      required this.beginTime,
      required this.endTime,
      required this.classType,
      required this.sourceLocation});
}
