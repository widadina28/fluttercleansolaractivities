import 'package:sun_flare_app/domain/models/geo_storm.dart';
import 'package:sun_flare_app/domain/models/solar_flare.dart';

class SolarActivities {
  final SolarFlare lastFlare;
  final GeoStorm lastStorm;

  SolarActivities({required this.lastFlare, required this.lastStorm});
}
