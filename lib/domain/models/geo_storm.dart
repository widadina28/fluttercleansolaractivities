import 'package:flutter/material.dart';

class GeoStorm {
  String? gstId = "";
  DateTime? startTime;
  GeoStorm({required this.gstId, required this.startTime});
}
