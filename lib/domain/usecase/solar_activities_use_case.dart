import 'package:sun_flare_app/domain/models/solar_activities.dart';
import 'package:sun_flare_app/domain/repository/geo_storm_repository.dart';
import 'package:sun_flare_app/domain/repository/solar_flare_repository.dart';

abstract class SolarActivitiesUseCase {
  Future<SolarActivities> getLastSolarActivities(
      GeoStormRepository geoStormRepository,
      SolarFlareRepository solarFlareRepository);
}
