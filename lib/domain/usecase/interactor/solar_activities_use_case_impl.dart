import 'dart:developer';

import 'package:sun_flare_app/domain/repository/solar_flare_repository.dart';
import 'package:sun_flare_app/domain/repository/geo_storm_repository.dart';
import 'package:sun_flare_app/domain/models/solar_activities.dart';
import 'package:sun_flare_app/domain/usecase/solar_activities_use_case.dart';

class SolarActivitiesUseCaseImpl implements SolarActivitiesUseCase {
  @override
  Future<SolarActivities> getLastSolarActivities(
      GeoStormRepository geoStormRepository,
      SolarFlareRepository solarFlareRepository) async {
    final fromDate = DateTime.now().subtract(Duration(days: 365));
    final toDate = DateTime.now();

    final storms =
        await geoStormRepository.getStorms(from: fromDate, to: toDate);
    final flares =
        await solarFlareRepository.getFlares(from: fromDate, to: toDate);
    log(flares.last.flrId.toString() + "last flareeeeee");
    log(storms.last.startTime.toString() + "last stormmmmm");
    return SolarActivities(lastFlare: flares.last, lastStorm: storms.last);
  }
}
