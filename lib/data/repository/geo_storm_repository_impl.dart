import 'package:sun_flare_app/data/service/nasa_service.dart';
import 'package:sun_flare_app/domain/models/geo_storm.dart';
import 'package:sun_flare_app/domain/repository/geo_storm_repository.dart';
import 'package:sun_flare_app/mapper/geo_storm_mapper.dart';

class GeoStormRepositoryImpl implements GeoStormRepository {
  final NasaService nasaService;

  GeoStormRepositoryImpl(this.nasaService);
  @override
  Future<List<GeoStorm>> getStorms(
      {required DateTime from, required DateTime to}) async {
    final res = await nasaService.getGeoStorms(from, to);
    return res.map((e) => e.toModel()).toList();
  }
}
