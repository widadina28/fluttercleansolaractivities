import 'package:sun_flare_app/data/service/nasa_service.dart';
import 'package:sun_flare_app/domain/models/solar_flare.dart';
import 'package:sun_flare_app/domain/repository/solar_flare_repository.dart';
import 'package:sun_flare_app/mapper/solar_flare_mapper.dart';

class SolarFlareRepositoryImpl implements SolarFlareRepository {
  final NasaService nasaService;

  SolarFlareRepositoryImpl(this.nasaService);
  @override
  Future<List<SolarFlare>> getFlares(
      {required DateTime from, required DateTime to}) async {
    final res = await nasaService.getFlares(from, to);
    return res.map((e) => e.toModel()).toList();
  }
}
