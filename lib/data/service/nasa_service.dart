import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:intl/intl.dart';
import 'package:sun_flare_app/data/remote/geo_storm_response.dart';
import 'package:sun_flare_app/data/remote/solar_flare_response.dart';

class NasaService {
  static const _BASE_URL = 'https://kauai.ccmc.gsfc.nasa.gov';

  final Dio _dio = Dio(
    BaseOptions(baseUrl: _BASE_URL),
  );

  Future<List<GeoStormResponse>> getGeoStorms(
      DateTime from, DateTime to) async {
    _dio.interceptors
        .add(LogInterceptor(requestBody: true, responseBody: true));
    final response = await _dio.get(
      '/DONKI/WS/get/GST',
      queryParameters: {
        'startDate': DateFormat('yyyy-MM-dd').format(from),
        'endDate': DateFormat('yyyy-MM-dd').format(to)
      },
    );
    return (response.data as List)
        .map((x) => GeoStormResponse.fromJson(x))
        .toList();
  }

  Future<List<SolarFlareResponse>> getFlares(DateTime from, DateTime to) async {
    _dio.interceptors
        .add(LogInterceptor(requestBody: true, responseBody: true));
    final response = await _dio.get(
      '/DONKI/WS/get/FLR',
      queryParameters: {
        'startDate': DateFormat('yyyy-MM-dd').format(from),
        'endDate': DateFormat('yyyy-MM-dd').format(to)
      },
    );

    return (response.data as List)
        .map((i) => SolarFlareResponse.fromJson(i))
        .toList();
  }
}
