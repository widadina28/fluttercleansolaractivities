// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'solar_flare_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SolarFlareResponse _$SolarFlareResponseFromJson(Map<String, dynamic> json) =>
    SolarFlareResponse(
      flrID: json['flrID'] as String?,
      beginTime: json['beginTime'] == null
          ? null
          : DateTime.parse(json['beginTime'] as String),
      endTime: json['endTime'] == null
          ? null
          : DateTime.parse(json['endTime'] as String),
      classType: json['classType'] as String?,
      sourceLocation: json['sourceLocation'] as String?,
      link: json['link'] as String?,
    );

Map<String, dynamic> _$SolarFlareResponseToJson(SolarFlareResponse instance) =>
    <String, dynamic>{
      'flrID': instance.flrID,
      'beginTime': instance.beginTime?.toIso8601String(),
      'endTime': instance.endTime?.toIso8601String(),
      'classType': instance.classType,
      'sourceLocation': instance.sourceLocation,
      'link': instance.link,
    };
