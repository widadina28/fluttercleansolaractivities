import 'package:json_annotation/json_annotation.dart';

part 'geo_storm_response.g.dart';

@JsonSerializable()
class GeoStormResponse {
  final String? gstId;
  final DateTime? startTime;
  final String? link;

  GeoStormResponse({this.gstId, this.startTime, this.link});
  factory GeoStormResponse.fromJson(Map<String, dynamic> json) =>
      _$GeoStormResponseFromJson(json);
  Map<String, dynamic> toJson() => _$GeoStormResponseToJson(this);
}
