// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'geo_storm_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GeoStormResponse _$GeoStormResponseFromJson(Map<String, dynamic> json) =>
    GeoStormResponse(
      gstId: json['gstId'] as String?,
      startTime: json['startTime'] == null
          ? null
          : DateTime.parse(json['startTime'] as String),
      link: json['link'] as String?,
    );

Map<String, dynamic> _$GeoStormResponseToJson(GeoStormResponse instance) =>
    <String, dynamic>{
      'gstId': instance.gstId,
      'startTime': instance.startTime?.toIso8601String(),
      'link': instance.link,
    };
