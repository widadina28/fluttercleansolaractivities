import 'package:json_annotation/json_annotation.dart';

part 'solar_flare_response.g.dart';

@JsonSerializable()
class SolarFlareResponse {
  final String? flrID;
  final DateTime? beginTime;
  final DateTime? endTime;
  final String? classType;
  final String? sourceLocation;
  final String? link;

  SolarFlareResponse(
      {this.flrID,
      this.beginTime,
      this.endTime,
      this.classType,
      this.sourceLocation,
      this.link});
  factory SolarFlareResponse.fromJson(Map<String, dynamic> json) =>
      _$SolarFlareResponseFromJson(json);
  Map<String, dynamic> toJson() => _$SolarFlareResponseToJson(this);
}
